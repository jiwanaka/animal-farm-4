#include <iostream>
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>


#include "node.hpp"
#include "list.hpp"

using namespace std;

namespace animalfarm{
    const bool SingleLinkedList::empty() const {
        bool status;
        if(head->next == NULL)
        status = true;
        else
        status = false;
        return status ;
    }

    void SingleLinkedList::push_front( Node* newNode ){
        newNode->next = nullptr;
        newNode->next = head;
        head = newNode;
        nodeCount++;
        }

    Node* SingleLinkedList::pop_front(){
        Node *tmp = head;
        head = head->next;
    //Node* temp = head;
    //head = head->next;
    //delete temp;
    return tmp;
}

    Node* SingleLinkedList:: get_first() const {
        return head;
    }

    Node* SingleLinkedList::get_next( const Node* currentNode ) const{
       return currentNode->next;
    }

   size_t SingleLinkedList::size() const{
        return nodeCount;
    }
}