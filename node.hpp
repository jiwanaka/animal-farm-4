#pragma once
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

namespace animalfarm{
    class Node{
        friend class SingleLinkedList;

        protected:
        Node* next = nullptr;
        size_t nodeCount = 0;

    };
}